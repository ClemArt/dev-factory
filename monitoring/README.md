# Swarm monitoring stack

## Requirements
* A running docker swarm
* SSH access to a swarm's manager
* docker 18.03+ on all machines
* Ansible 2.5.3+

## Stack content and placement constraint
* Prometheus
    * node.role == manager
    * node.labels.prometheus == true 
* Grafana
    * node.role == manager
    * node.labels.grafana == true 
* Prometheus' node-exporter

## Volume management
Prometheus stores its data in the prometheus-data local volume
Grafana stores its data in the grafana-data local volume.
As such they should be used on a well identified manager node. Moving the container to another node results in a data reset.

Further improvements will be taken to add replication configuration to both.

## Deployment
    ansible-playbook -i <inventory> monitoring/playbook.yml -v

Variables to surcharge

|Variable|Description|
|-|-|
|grafana_admin_user|Admin user to initialize grafana|
|grafana_admin_password|Password for the Admin user|